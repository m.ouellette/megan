/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arraypractice;

/**
 *
 * Megan Ouellette
 * 12/9/2018
 */
public class ArrayPractice {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] a = new int[6];
        a[0] = 5;
        a[1] = 7;
        a[2] = 3;
        a[3] = 2;
        a[4] = 9;
        a[5] = 6;
        largestSmallest(a);
        //--------------------------
        int[] b = new int[15];
        b[0] = 5;
        b[1] = 7;
        b[2] = 3;
        b[3] = 2;
        b[4] = 9;
        b[5] = 6;
        b[6] = 9;
        b[7] = 3;
        b[8] = 4;
        b[9] = 3;
        b[10] = 8;
        b[11] = 11;
        b[12] = 9;
        b[13] = 6;
        b[14] = 3;
        occurences(b,3);
        //--------------------------
        int[] d = new int[12];
        d[0] = 2;
        d[1] = 6;
        d[2] = -8;
        d[3] = 5;
        d[4] = -4;
        d[5] = -3;
        d[6] = 1;
        d[7] = -8;
        d[8] = 4;
        d[9] = -1;
        d[10] = 7;
        d[11] = 10;
        positiveNegative(d);
        //----------------------------
        double[] c = new double[5];
        c[0] = 2.3;
        c[1] = 6.73;
        c[2] = 4.01;
        c[3] = 3.999;
        c[4] = 7.5;
        
        sumAverage(c);
    }
    public static void largestSmallest(int[] a) {
        int l = a[0];
        int s = a[0];
        for(int i = 0; i < a.length; i++){
            if (a[i] > l){
                l = a[i];
            }
            if (a[i] < s){
                s = a[i];
            }
        }
        System.out.println("Largest: " + l + " Smallest: " + s);
    }
    
    public static void occurences(int[] b, int k){
        int occur = 0;
        int less = 0;
        int greater = 0;
        for(int i = 0; i < b.length; i++){
            if (b[i] == k){
                occur++;
            }else if (b[i] > k){
                greater++;      
            }else{
                less++;
            }
           
        }
        System.out.println("k count: " + occur + " Less than k count: " + less + " Greater than k count: " + greater);
    }
    
    public static void longestShortest(String[] w){
        String longest = w[0];
        int lpos = 0;
        String shortest = w[0];
        int spos = 0;
        for (int i = 0; i < w.length; i++){
          if(w[i].length() > longest.length()){
              longest = w[i];
              lpos = i;
             
          } else if (w[i].length() < shortest.length()){
              shortest = w[i];
              spos = i;
          }
        }
        System.out.println("Longest: " + longest + " Position: " + lpos + " Shortest: " + shortest + "Position:" + spos);
    }
    
    public static void positiveNegative(int[] j){
        int pos = 0;
        int neg = 0;
        for (int i = 0; i < j.length; i++){
            if (j[i] > 0){
                pos++;
            }else if (j[i] < 0){
                neg++;
            }
        
        }
        System.out.println("Positive Count: " + pos + " Negative Count: " + neg);
    }
    
    
    
 
    
    
    
    
    public static void sumAverage(double[] a){
        double sum = 0;
        double average = 0;
        for (int i = 0; i < a.length; i++){
            sum += a[i];
        }
        for (int i = 0; i < a.length; i++){
            average += a[i];
            
            
        }
        System.out.println("Sum: " + sum + " average: " + average/a.length);
    }
}
